import { LitElement, html } from '@polymer/lit-element'
import {style} from './main-styles.js';

export class MyTitle extends LitElement {
  constructor() {
    super()
  }
  render(){
    return html`
      ${style}
      <h1 class="title">
        Kitchen Sink 🍔 [LitElement]
      </h1> 
    `
  }
}
customElements.define('my-title', MyTitle)