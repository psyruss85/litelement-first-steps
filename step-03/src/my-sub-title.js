import { LitElement, html } from '@polymer/lit-element';
import {style} from './main-styles.js';

export class MySubTitle extends LitElement {
  constructor() {
    super()
  }

  render(){
    return html`
      ${style}
      <h2 class="subtitle">
        Training with 🦊 GitLab is fun
      </h2>  
      <div> 
        <input placeholder="type your text" type="text" class="field">
      </div>
      <div>
        <a href="something" class="button">Button</a>
        <a href="something" class="button">Another button</a> 
      </div>
    `
  }
}
customElements.define('my-sub-title', MySubTitle)