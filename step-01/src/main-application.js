import { LitElement, html } from '@polymer/lit-element';
import './my-title.js'
import './my-sub-title.js'

export class MainApplication extends LitElement {

  constructor() {
    super()
  }

  render() {
    return html`
      <section>
        <div>
          <h1>Hello World</h1>
          <my-title></my-title>
          <my-sub-title></my-sub-title>
        </div>
      </section>
    `
  }

}

customElements.define('main-application', MainApplication);
