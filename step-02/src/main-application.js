import { LitElement, html } from '@polymer/lit-element';
import './my-title.js'
import './my-sub-title.js'

export class MainApplication extends LitElement {

  constructor() {
    super()
  }

  render() {
    return html`
      <style>
        .container
          {
            min-height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
          }
      </style>
      <section class="container">
        <div>
          <my-title></my-title>
          <my-sub-title></my-sub-title>
        </div>
      </section>
    `
  }

}

customElements.define('main-application', MainApplication);
